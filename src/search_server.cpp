#include "search_server.h"
#include "iterator_range.h"

#include <algorithm>
#include <iterator>
#include <future>
#include <sstream>
#include <iostream>
#include <array>

vector<string> SplitIntoWords(const string& line) {
  istringstream words_input(line);
  return {istream_iterator<string>(words_input), istream_iterator<string>()};
}

SearchServer::SearchServer(istream& document_input) {
  UpdateDocumentBase(document_input);
}

void SearchServer::UpdateDocumentBase(istream& document_input) {
	std::async(std::launch::async, [&] {
		  InvertedIndex new_index;

		  for (string current_document; getline(document_input, current_document); ) {
		    new_index.Add(move(current_document));
		  }

		  lock_guard<mutex> locker(m);
		  index = move(new_index);
	});
}

void SearchServer::AddQueriesStream(
  istream& query_input, ostream& search_results_output
) {
	lock_guard<mutex> locker(m);
	size_t documentsCount = index.GetDocumentsCount();

	if (documentsCount == 0)
	{
		return;
	}

	vector<pair<size_t, size_t>> search_results(documentsCount);

  for (string current_query; getline(query_input, current_query); ) {
	  std::fill(search_results.begin(), search_results.end(), make_pair<size_t, size_t>(0, 0));

	  map<string, size_t> wordsToCount;

	  for (const auto& word : SplitIntoWords(current_query)) {
		  wordsToCount[word]++;
	  }


    for (const auto& [word, countInQuery] : wordsToCount) {
      for (const auto & [docid, countInDocument] : index.Lookup(word)) {
    	  search_results[docid].first = docid;
    	  search_results[docid].second += countInDocument * countInQuery;
      }
    }

    const int outputRowsCount = 5;

    auto middleIterator = search_results.size() <= outputRowsCount ? end(search_results)
    		                                                       : next(begin(search_results), outputRowsCount);

    partial_sort(
      begin(search_results),
	  middleIterator,
      end(search_results),
      [](pair<size_t, size_t> lhs, pair<size_t, size_t> rhs) {
        int64_t lhs_docid = lhs.first;
        auto lhs_hit_count = lhs.second;
        int64_t rhs_docid = rhs.first;
        auto rhs_hit_count = rhs.second;
        return make_pair(lhs_hit_count, -lhs_docid) > make_pair(rhs_hit_count, -rhs_docid);
      }
    );

    search_results_output << current_query << ':';
    for (auto [docid, hitcount] : Head(search_results, outputRowsCount)) {
      if (hitcount == 0)
      {
    	  break;
      }
      search_results_output << " {"
        << "docid: " << docid << ", "
        << "hitcount: " << hitcount << '}';
    }
    search_results_output << endl;
  }
}

void InvertedIndex::Add(const string& document) {
  docs.push_back(document);

  const size_t docid = docs.size() - 1;

  map<string, size_t> wordsToCount;

  for (const auto& word : SplitIntoWords(document)) {
	  wordsToCount[word]++;
  }

  for (auto & [word, count] : wordsToCount)
  {
	  index[word].push_back({docid, count});
  }
}

vector<pair<size_t, size_t>> InvertedIndex::Lookup(const string& word) const {
  if (auto it = index.find(word); it != index.end()) {
    return it->second;
  } else {
    return {};
  }
}
